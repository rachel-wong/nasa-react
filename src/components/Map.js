import { useState} from 'react'
import GoogleMapReact from 'google-map-react';
import LocationMarker from './LocationMarker'
import LocationInfoBox from './LocationInfoBox'

const NATURAL_EVENT_WILDFIRE = 8;

const Map = ({ eventData, center, zoom }) => {

  const [locationInfo, setLocationInfo] = useState(null)
  const [displayState, setDisplayState] = useState(false)

  // const markers = eventData.filter(x => x.categories[0].id === 8)
  const markers = eventData.map((x, idx) => {
    if (x.categories[0].id === NATURAL_EVENT_WILDFIRE) {
      return <LocationMarker key={idx} lat={x.geometries[0].coordinates[1]} lng={x.geometries[0].coordinates[0]} onClick={() => setLocationInfo({ id: x.id, title: x.title })}/>
    }
  })

  return (
    <div className="map">
      <GoogleMapReact
        bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
        defaultCenter={center}
        defaultZoom={zoom}>

        { markers.length > 0 && markers }
        { /* <LocationMarker lat={center.lat} lng={ center.lng}/> */ }
      </GoogleMapReact>
      {locationInfo && <LocationInfoBox info={locationInfo} displayBox={() => { setDisplayState(!displayState) }} />}
    </div>
  )
}

Map.defaultProps = {
  center: {
    lat: 42.3265,
    lng: -122.8756
  },
  zoom: 6
}

export default Map

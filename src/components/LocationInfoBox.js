import { useState } from 'react'

const LocationInfoBox = ({ info, displayBox }) => {
  return (
    // <div className={`location-info ${displayBox ? 'location-info__close' : 'location-info__open'}` }>
    <div className={`location-info`}>
      <h2>Event Location Info</h2>
      <ul>
        <li>ID: <strong>{info.id}</strong></li>
        <li>Title: <strong>{info.title}</strong></li>
      </ul>
      {/* <button className="btn" onClick={ !displayBox }>Close</button> */}
    </div>
  )
}

export default LocationInfoBox
